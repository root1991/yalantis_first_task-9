package com.example.dwork.csi;

import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;


/**
 * Created by Korzch Alexandr on 3/13/2016.
 */
public class MyApplication extends Application {

    public static int displayWidth;

    @Override
    public void onCreate() {
        super.onCreate();
        Fresco.initialize(this);
    }
}